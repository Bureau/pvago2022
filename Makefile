#!/usr/bin/make -f
#
# Requires latexmk.
#
# Maxime Bombar <bombar@crans.org>

## This Makefile is used to build pdf from a latex project. Use ```make help``` to display a summary of the rules.
##
## make MyFancyProject.pdf	: Builds MyFancyProject.pdf (and auxiliary files) from MyFancyProject.tex and fails if it doesn't exist.
## make MyFancyProject.warn	: Displays how many warnings and bad boxes there are when compiling MyFancyProject.
## make MyFancyProject.showwarn	: Displays the warnings and bad boxes for MyFancyProject.

.PHONY: all help clean cleanall purge
SHELL = /bin/bash


# latexmk is a swiss army knife of latex compilation. Option -pdf to output the pdf.

ifeq (,$(shell which latexmk))
$(warning "[Warning] Latexmk not found. Using pdflatex instead.")
PDFLATEX := pdflatex
else
PDFLATEX := latexmk -pdf -shell-escape
endif

CMD = cat $*.log | grep -iE
REGEXBOX := full..hbox
WARNINGS := warning


TEXDOCS = $(wildcard *.tex)
PDF := $(patsubst %.tex,%.pdf, $(TEXDOCS))
OUT := $(patsubst %.tex,%.out, $(TEXDOCS))
LOG := $(patsubst %.tex,%.log, $(TEXDOCS))


# Prevents some intermediate files to be automatically deleted...
.PRECIOUS: %.log %.pdf Makefile

%.log: %.tex
	$(PDFLATEX) $<

%.warn: %.log
	@echo "${shell echo 'There are $$(( $$($(CMD) $(WARNINGS) | wc -l)-1 ))' warning\(s\)}"
	@echo "${shell echo 'There are $$($(CMD) $(REGEXBOX) | wc -l)' bad boxe\(s\)}"

%.showwarn: %.log
	@echo "${shell echo 'There are $$(( $$($(CMD) $(WARNINGS) | wc -l)-1 ))' warning\(s\) for $*:}"
	@echo "${shell echo '$$( $(CMD) $(WARNINGS) | tail +2)' }"
	@echo "${shell echo $(\n)}"
	@echo "${shell echo 'There are $$($(CMD) $(REGEXBOX) | wc -l)' bad boxe\(s\) for $*:}"
	@echo "${shell echo '$$( $(CMD) $(REGEXBOX))' }"

%.pdf: %.log
	@echo "${shell echo 'There are $$(( $$($(CMD) $(WARNINGS) | wc -l)-1 ))' warning\(s\)}"
	@echo "${shell echo 'There are $$($(CMD) $(REGEXBOX) | wc -l)' bad boxe\(s\)}"

## make all			: Builds every file in the current directory.
all: $(TEXDOCS)
	cd ./Bilans/2022/; $(PDFLATEX) bilan-moral.tex; $(PDFLATEX) bilan-financier.tex; $(PDFLATEX) bilan-technique.tex; rm -f *.out
	$(PDFLATEX) $^


## make rebuild			: Cleans and rebuilds every file.
rebuild: cleanall all

## make clean			: Removes every auto-generated file except for pdf.
clean:
	@echo -n "aux bbl bcf blg dvi fdb_latexmk fls log nav snm synctex.gz tdo toc thm vrb xml"|xargs -t -d ' ' -I {} find . -iname "*.{}" -delete
	rm -f $(OUT)
	rm -f $(LOG)
	find . -iname "*flymake*" -delete
	find . -iname "*~" -delete
	find . -iname "\#*" -delete
	find . -type d  -iname 'auto' -exec rm -rf {} +
	find . -type d  -iname '.auctex-auto' -exec rm -rf {} +

## make purge			: Removes every auto-generated file.
purge: clean
	rm -f $(PDF)
	rm -rf Bilans/*/*.pdf

# For retrocompatibility
cleanall: purge

## make help			: Displays this help.
help: Makefile
	@sed -n 's/^##//p' $<
